<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:35
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-04 10:29:20
*/

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rujukan;

/**
 * RujukanSearch represents the model behind the search form of `app\models\Rujukan`.
 */
class RujukanSearch extends Rujukan
{


    public $asal_rujukan_text;
    public $tujuan_rujukan_text;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nik', 'no_bpjs_jkd', 'asal_rujukan', 'tujuan_rujukan', 'nadi', 'pernapasan'], 'integer'],
            [['tgl_masuk', 'jenis', 'nama', 'jk', 'tplahir', 'tglahir', 'umur', 'alamat', 'cara_bayar', 'alasan_rujukan', 'anamnesa', 'kesadaran', 'tekanan_darah', 'suhu', 'nyeri', 'pemeriksaan_fisik', 'pemeriksaan_penunjang', 'diagnosa', 'tindakan_yg_sdh_diberikan', 'info_balik', 'status'], 'safe'],
            [['asal_rujukan_text', 'tujuan_rujukan_text'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $idLogin)
    {
        $urutBy[] = new \yii\db\Expression('status = "Menunggu" desc');
        $urutBy[] = new \yii\db\Expression('id desc');

        $query = Rujukan::find()
                ->where(['=', 'asal_rujukan', $idLogin])
                ->orderBy($urutBy);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'tgl_masuk' => $this->tgl_masuk,
            'tglahir' => $this->tglahir,
            'nik' => $this->nik,
            'no_bpjs_jkd' => $this->no_bpjs_jkd,
            'asal_rujukan' => $this->asal_rujukan,
            'tujuan_rujukan' => $this->tujuan_rujukan,
            'nadi' => $this->nadi,
            'pernapasan' => $this->pernapasan,
        ]);

        $query->andFilterWhere(['like', 'jenis', $this->jenis])
            ->andFilterWhere(['like', 'tgl_masuk', $this->tgl_masuk])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jk', $this->jk])
            ->andFilterWhere(['like', 'tplahir', $this->tplahir])
            ->andFilterWhere(['like', 'umur', $this->umur])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'cara_bayar', $this->cara_bayar])
            ->andFilterWhere(['like', 'alasan_rujukan', $this->alasan_rujukan])
            ->andFilterWhere(['like', 'anamnesa', $this->anamnesa])
            ->andFilterWhere(['like', 'kesadaran', $this->kesadaran])
            ->andFilterWhere(['like', 'tekanan_darah', $this->tekanan_darah])
            ->andFilterWhere(['like', 'suhu', $this->suhu])
            ->andFilterWhere(['like', 'nyeri', $this->nyeri])
            ->andFilterWhere(['like', 'pemeriksaan_fisik', $this->pemeriksaan_fisik])
            ->andFilterWhere(['like', 'pemeriksaan_penunjang', $this->pemeriksaan_penunjang])
            ->andFilterWhere(['like', 'diagnosa', $this->diagnosa])
            ->andFilterWhere(['like', 'tindakan_yg_sdh_diberikan', $this->tindakan_yg_sdh_diberikan])
            ->andFilterWhere(['like', 'info_balik', $this->info_balik])
            ->andFilterWhere(['like', 'status', $this->status]);

        if($this->asal_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'asal_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->asal_rujukan_text])]);
        }
        if($this->tujuan_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'tujuan_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->tujuan_rujukan_text])]);
        }

        return $dataProvider;
    }

    public function searchMonitoring($params)
    {
        $urutBy[] = new \yii\db\Expression('status = "Menunggu" desc');
        $urutBy[] = new \yii\db\Expression('id desc');

        $query = Rujukan::find()
                ->orderBy($urutBy);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'tgl_masuk' => $this->tgl_masuk,
            'tglahir' => $this->tglahir,
            'nik' => $this->nik,
            'no_bpjs_jkd' => $this->no_bpjs_jkd,
            'asal_rujukan' => $this->asal_rujukan,
            'tujuan_rujukan' => $this->tujuan_rujukan,
            'nadi' => $this->nadi,
            'pernapasan' => $this->pernapasan,
        ]);

        $query->andFilterWhere(['like', 'jenis', $this->jenis])
            ->andFilterWhere(['like', 'tgl_masuk', $this->tgl_masuk])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jk', $this->jk])
            ->andFilterWhere(['like', 'tplahir', $this->tplahir])
            ->andFilterWhere(['like', 'umur', $this->umur])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'cara_bayar', $this->cara_bayar])
            ->andFilterWhere(['like', 'alasan_rujukan', $this->alasan_rujukan])
            ->andFilterWhere(['like', 'anamnesa', $this->anamnesa])
            ->andFilterWhere(['like', 'kesadaran', $this->kesadaran])
            ->andFilterWhere(['like', 'tekanan_darah', $this->tekanan_darah])
            ->andFilterWhere(['like', 'suhu', $this->suhu])
            ->andFilterWhere(['like', 'nyeri', $this->nyeri])
            ->andFilterWhere(['like', 'pemeriksaan_fisik', $this->pemeriksaan_fisik])
            ->andFilterWhere(['like', 'pemeriksaan_penunjang', $this->pemeriksaan_penunjang])
            ->andFilterWhere(['like', 'diagnosa', $this->diagnosa])
            ->andFilterWhere(['like', 'tindakan_yg_sdh_diberikan', $this->tindakan_yg_sdh_diberikan])
            ->andFilterWhere(['like', 'info_balik', $this->info_balik])
            ->andFilterWhere(['like', 'status', $this->status]);

        if($this->asal_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'asal_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->asal_rujukan_text])]);
        }
        if($this->tujuan_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'tujuan_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->tujuan_rujukan_text])]);
        }

        return $dataProvider;
    }

    public function searchLaporan($params, $awal=null, $akhir=null)
    {
        // $urutBy[] = new \yii\db\Expression('status = "Menunggu" desc');
        // $urutBy[] = new \yii\db\Expression('id desc');

// return $awal;

        if($awal===null && $akhir===null){
            $query = Rujukan::find()
                // ->where(['between', 'tgl_masuk', $awal, '2018-05-12' ])
                // ->where(['between', 'tgl_masuk', '2018-05-12', '2018-05-12' ])
                ->where(['!=', 'status', 'Menunggu'])
                ->orderBy('tgl_masuk desc');
                // ->orderBy($urutBy);
        }else{
            $query = Rujukan::find()
                // ->where(['between', 'tgl_masuk', $awal, '2018-05-12' ])
                // ->where(['between', 'tgl_masuk', '2018-05-12', '2018-05-12' ])
                ->where(['between', 'tgl_masuk', (string)$awal, (string)$akhir ])
                ->andFilterWhere(['!=', 'status', 'Menunggu'])
                ->orderBy('tgl_masuk asc');
                // ->orderBy($urutBy);
        }

        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'tgl_masuk' => $this->tgl_masuk,
            'tglahir' => $this->tglahir,
            'nik' => $this->nik,
            'no_bpjs_jkd' => $this->no_bpjs_jkd,
            'asal_rujukan' => $this->asal_rujukan,
            'tujuan_rujukan' => $this->tujuan_rujukan,
            'nadi' => $this->nadi,
            'pernapasan' => $this->pernapasan,
        ]);

        $query->andFilterWhere(['like', 'jenis', $this->jenis])
            ->andFilterWhere(['like', 'tgl_masuk', $this->tgl_masuk])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jk', $this->jk])
            ->andFilterWhere(['like', 'tplahir', $this->tplahir])
            ->andFilterWhere(['like', 'umur', $this->umur])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'cara_bayar', $this->cara_bayar])
            ->andFilterWhere(['like', 'alasan_rujukan', $this->alasan_rujukan])
            ->andFilterWhere(['like', 'anamnesa', $this->anamnesa])
            ->andFilterWhere(['like', 'kesadaran', $this->kesadaran])
            ->andFilterWhere(['like', 'tekanan_darah', $this->tekanan_darah])
            ->andFilterWhere(['like', 'suhu', $this->suhu])
            ->andFilterWhere(['like', 'nyeri', $this->nyeri])
            ->andFilterWhere(['like', 'pemeriksaan_fisik', $this->pemeriksaan_fisik])
            ->andFilterWhere(['like', 'pemeriksaan_penunjang', $this->pemeriksaan_penunjang])
            ->andFilterWhere(['like', 'diagnosa', $this->diagnosa])
            ->andFilterWhere(['like', 'tindakan_yg_sdh_diberikan', $this->tindakan_yg_sdh_diberikan])
            ->andFilterWhere(['like', 'info_balik', $this->info_balik])
            ->andFilterWhere(['like', 'status', $this->status]);

        if($this->asal_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'asal_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->asal_rujukan_text])]);
        }
        if($this->tujuan_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'tujuan_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->tujuan_rujukan_text])]);
        }

        //khusus untuk laporan user
        if(Yii::$app->user->identity->hak_akses==='user'){
            $query->andFilterWhere(['=', 'asal_rujukan', Yii::$app->user->identity->id]);
        }

        return $dataProvider;
    }
}
