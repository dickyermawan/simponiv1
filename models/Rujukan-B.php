<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-05 08:42:14
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-06 15:54:03
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "rujukan".
 *
 * @property int $id
 * @property string $ibu_nama
 * @property string $ibu_tplahir
 * @property string $ibu_tglahir
 * @property int $ibu_umur
 * @property string $ibu_alamat
 * @property string $ibu_carabayar
 * @property string $bayi_jk
 * @property string $bayi_nama
 * @property string $bayi_tplahir
 * @property string $bayi_tglahir
 * @property int $bayi_umur
 * @property string $bayi_alamat
 * @property string $bayi_carabayar
 * @property string $asal_rujukan
 * @property string $tujuan_rujukan
 * @property string $tindakan_ygdiberikan
 * @property string $alasan_rujukan
 * @property string $diagnosa
 * @property string $kesadaran
 * @property string $tekanan_darah
 * @property string $nadi
 * @property double $suhu
 * @property string $pernapasan
 * @property int $berat_badan
 * @property int $tinggi_badan
 * @property string $lila
 * @property string $nyeri
 * @property string $keterangan_lain
 * @property string $info_balik
 * @property string $status
 */
class Rujukan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $asal_r;
    public $tujuan_r;

    public CONST MENUNGGU = 'Menunggu';
    public CONST DITERIMA = 'Diterima';
    public CONST TIDAK_DITERIMA = 'Tidak Diterima';

    public static function tableName()
    {
        return 'rujukan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['ibu_nama', 'ibu_tplahir', 'ibu_tglahir', 'ibu_umur', 'ibu_alamat', 'ibu_carabayar', 'bayi_jk', 'bayi_nama', 'bayi_tplahir', 'bayi_tglahir', 'bayi_umur', 'bayi_alamat', 'bayi_carabayar', 'asal_rujukan', 'tujuan_rujukan', 'tindakan_ygdiberikan', 'alasan_rujukan', 'diagnosa', 'kesadaran', 'tekanan_darah', 'nadi', 'suhu', 'pernapasan', 'berat_badan', 'tinggi_badan', 'lila', 'nyeri', 'keterangan_lain', 'info_balik', 'status'], 'required'],

            [['ibu_nama', 'ibu_tplahir', 'tgl_masuk', 'ibu_tglahir', 'ibu_umur', 'ibu_alamat', 'ibu_carabayar', 'asal_rujukan', 'tujuan_rujukan', 'tindakan_ygdiberikan', 'alasan_rujukan', 'diagnosa', 'status'], 'required', 'message' => '{attribute} tidak boleh kosong.'],
            [['info_balik'], 'required', 'on' => 'prosesRujukan', 'message' => '{attribute} tidak boleh kosong.'],
            [['tgl_masuk', 'ibu_tglahir', 'bayi_tglahir', 'asal_r', 'tujuan_r',], 'safe'],
            [['ibu_alamat', 'bayi_alamat', 'tindakan_ygdiberikan', 'alasan_rujukan', 'diagnosa', 'info_balik', 'keterangan_lain'], 'string'],
            [['suhu'], 'number'],
            [['berat_badan', 'tinggi_badan', 'asal_rujukan', 'tujuan_rujukan'], 'integer'],
            [['ibu_nama', 'ibu_tplahir', 'ibu_carabayar', 'bayi_nama', 'bayi_carabayar'], 'string', 'max' => 150],
            [['ibu_umur', 'bayi_umur'], 'string', 'max' => 2],
            [['bayi_jk', 'nadi', 'pernapasan', 'nyeri', 'status'], 'string', 'max' => 25],
            [['bayi_tplahir'], 'string', 'max' => 11],
            // [['asal_rujukan'], 'string', 'max' => 150],
            // [['tujuan_rujukan'], 'string', 'max' => 250],
            [['kesadaran'], 'string', 'max' => 12],
            [['tekanan_darah'], 'string', 'max' => 10],
            [['lila'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tgl_masuk' => 'Tanggal',
            'ibu_nama' => 'Nama Pasien',
            'ibu_tplahir' => 'Tempat lahir',
            'ibu_tglahir' => 'Tanggal lahir',
            'ibu_umur' => 'Umur',
            'ibu_alamat' => 'Alamat',
            'ibu_carabayar' => 'Cara bayar',
            'bayi_jk' => 'Jenis Kelamin',
            'bayi_nama' => 'Nama Pasien',
            'bayi_tplahir' => 'Tempat lahir',
            'bayi_tglahir' => 'Tanggal lahir',
            'bayi_umur' => 'Umur',
            'bayi_alamat' => 'Alamat',
            'bayi_carabayar' => 'Cara bayar',
            'asal_rujukan' => 'Asal Rujukan',
            'asal_rujukan_text' => 'Asal Rujukan',
            'tujuan_rujukan_text' => 'Tujuan Rujukan',
            'tujuan_rujukan' => 'Tujuan Rujukan',
            'tindakan_ygdiberikan' => 'Tindakan yang sudah diberikan',
            'alasan_rujukan' => 'Alasan Rujukan',
            'diagnosa' => 'Diagnosa',
            'kesadaran' => 'Kesadaran',
            'tekanan_darah' => 'Tekanan Darah',
            'nadi' => 'Nadi',
            'suhu' => 'Suhu',
            'pernapasan' => 'Pernapasan',
            'berat_badan' => 'Berat Badan',
            'tinggi_badan' => 'Tinggi Badan',
            'lila' => 'Lila',
            'nyeri' => 'Nyeri',
            'keterangan_lain' => 'Keterangan Lain',
            'info_balik' => 'Info Balik',
            'status' => 'Status',
        ];
    }

    public function attributeHints()
    {
        return array(
                'ibu_nama'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'ibu_tplahir'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'ibu_tglahir'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'ibu_umur'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'ibu_alamat'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'ibu_carabayar'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'tindakan_ygdiberikan'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'alasan_rujukan'=>'<span style="color:#e07734">*Harus diisi.</span>',
                'diagnosa'=>'<span style="color:#e07734">*Harus diisi.</span>',
        );
    }

    public function getRelasinamaasal()
    {
        return $this->hasOne(Pengguna::className(), ['id' => 'asal_rujukan']);
    }
    public function getAsal_rujukan_text() 
    {
        return $this->relasinamaasal->nama_rs_puskesmas;
    }

    public function getRelasinamatujuan()
    {
        return $this->hasOne(Pengguna::className(), ['id' => 'tujuan_rujukan']);
    }
    public function getTujuan_rujukan_text() 
    {
        return $this->relasinamatujuan->nama_rs_puskesmas;
    }
}
