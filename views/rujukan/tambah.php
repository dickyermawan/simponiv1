<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-11 17:37:35
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-11 18:03:52
*/

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rujukan */

$this->title = 'Tambah Rujukan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rujukan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
